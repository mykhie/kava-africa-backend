<?php
/**
 * Created by PhpStorm.
 * User: Osoro
 * Date: 12/9/2019
 * Time: 09:43
 */


class utility
{

	public $maximumIncCodelen = 8;

	public function echoJsonArray($status, $message = null, $data = null, $totalRecords = 0)
	{
		echo $this->returnJsonArray(array('status' => $status, 'message' => $message, 'data' => $data, 'totalRecord' => $totalRecords));
		exit;
	}


	public function returnJsonArray($dataArray)
	{
		if (sizeof($dataArray) > 0) {
			return json_encode($dataArray);
		}

		return (json_encode(array('status' => 0)));

	}


	public function returnArrayFromInput()
	{
		$decodeJson = file_get_contents("php://input");
		$request = json_decode($decodeJson);
		$request = (array)$request;
		return (array)$request['saveData'];
	}


}
