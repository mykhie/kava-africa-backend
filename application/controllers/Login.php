<?php
header('Access-Control-Allow-Origin: *');

header('Access-Control-Allow-Methods: GET, POST');

header("Access-Control-Allow-Headers: *");

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		// call you middle where if need be
	}

	public function userLogin()
	{
		$loginDetails = $this->utility->returnArrayFromInput();

		if (is_array($loginDetails)) {
			$where = array(
				"email" => $loginDetails['email'],
				"password" => md5($loginDetails['password'])
			);
			$userDetails = $this->Loginmodel->userLogin($where);
			//var_dump($userDetails);

			if ($userDetails == null) {
				$this->utility->echoJsonArray(0, "Incorrect email or password");
			}
			// generate a token and store

			$this->utility->echoJsonArray(1, "Successful login", $userDetails);
		}
		$this->utility->echoJsonArray(0, "Incorrect username or password");
	}

	public function userRegistration()
	{
		$loginDetails = $this->utility->returnArrayFromInput();

		if (is_array($loginDetails)) {
			$insert = array(
				"fullname" => $loginDetails['fullname'],
				"email" => $loginDetails['email'],
				"password" => md5($loginDetails['password'])
			);
			// check for email duplicates
			$whereEmail['email'] = $insert['email'];
			$user = $this->Usersmodel->userDetails($whereEmail);
			if ($user != null) {
				$this->utility->echoJsonArray(0, "Email is already in use,please use a different email");
			}
			if ($this->Loginmodel->save($insert) > 0) {
				$this->utility->echoJsonArray(1, "Successful registration,please click below to log in", $loginDetails);
			}
		}
		$this->utility->echoJsonArray(0, "An error occurred,could not create account,please try again");
	}

}
