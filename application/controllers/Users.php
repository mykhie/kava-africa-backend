<?php


class Users extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		// call you middle where if need be
	}

	// the user id can be resolved from the token issued to the user
	public function updateUserDetails($userId)
	{
		$loginDetails = $this->utility->returnArrayFromInput();

		if (is_array($loginDetails)) {
			// update information
			//
			$where = array('id' => $userId);
			$up = array(
				"fullname" => $loginDetails['fullname'],
				"email" => $loginDetails['email'],
			);
			$this->Usersmodel->update($where, $up);
			$user = $this->Usersmodel->userDetails($where);
			$this->utility->echoJsonArray(1, "Profile has been updated", $user);
		}
		$this->utility->echoJsonArray(0, "Server error occurred,please try again");
	}
}
