<?php
/**
 * Created by PhpStorm.
 * User: Osoro
 * Date: 12/9/2019
 * Time: 09:47
 */

class Usersmodel extends CI_Model
{

    public $table = 'kava_users';

    public function __construct()
    {
        parent::__construct();
    }


    public function save($in)
    {

        try {
            $this->db->insert($this->table, $in);
            $insert_id = $this->db->insert_id();
        } catch (Exception $e) {
            $insert_id = null;
        }
        return $insert_id;
    }

    public function update($where, $up)
    {

        $query = $this->db->where($where)->update($this->table, $up);
        if ($query) {
            return true;
        } else {
            return FALSE;
        }
    }

	public function userDetails($whr)
	{


		$this->db->select('id,email,fullname')
			->where($whr);

		return $this->db->get("kava_users")
			->row();

	}

}
