<?php


class Loginmodel extends CI_Model
{
	public $table = 'kava_users';
	public function __construct()
	{
		parent::__construct();
	}

	public function userLogin($whr)
	{


		$this->db->select('id,email,fullname')
			->where($whr);

		return $this->db->get($this->table)
			->row();

	}

	public function save($in)
	{

		try {
			$this->db->insert($this->table, $in);
			$insert_id = $this->db->insert_id();
		} catch (Exception $e) {
			$insert_id = null;
		}
		return $insert_id;
	}
}
