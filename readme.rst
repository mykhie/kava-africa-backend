###################
About this app
###################

This is a simple app built with codeigniter(php 7.2) and angularjs.
All the data i accessed via by unauthorized APIs

**************************
Database Connections
**************************
On the root of the project,there is a file ```config.json``` that has DB credentials.
They can be changed to match the your current database

On the root folder,there is a  database/database.sql file.Run that against you database so as to generate tables and populate with dummy data


You can now access the app via the browser e.g http://localhost:9000/

