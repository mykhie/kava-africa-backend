-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2021 at 09:23 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kava_africa`
--

-- --------------------------------------------------------

--
-- Table structure for table `kava_users`
--

CREATE TABLE `kava_users` (
  `id` int(11) NOT NULL,
  `email` varchar(55) DEFAULT NULL,
  `fullname` varchar(45) DEFAULT NULL,
  `password` varchar(105) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kava_users`
--

INSERT INTO `kava_users` (`id`, `email`, `fullname`, `password`, `status`) VALUES
(1, 'kula@gmail.com', 'James Kamau', '85eb3a0ed27cf97d6dc3878c60f022f8', 1),
(2, 'osoro@gmail.com', 'osoro', 'c4ca4238a0b923820dcc509a6f75849b', NULL),
(3, 'osorom@gmail.com', 'Osoro', 'c4ca4238a0b923820dcc509a6f75849b', NULL),
(4, 'osoromichael@gmail.com', 'James', 'c4ca4238a0b923820dcc509a6f75849b', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kava_users_tokens`
--

CREATE TABLE `kava_users_tokens` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `token` varchar(75) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `expiryDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kava_users`
--
ALTER TABLE `kava_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kava_users_tokens`
--
ALTER TABLE `kava_users_tokens`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kava_users`
--
ALTER TABLE `kava_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kava_users_tokens`
--
ALTER TABLE `kava_users_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
